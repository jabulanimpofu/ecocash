package co.neolab.ecocash;

/**
 * Created by jabulanimpofu on 22/5/2017.
 */

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public enum Config {
    INSTANCE;

    private String beginIndex;
    private String endIndex;
    private String[] smsSenderNumbers;

    public void initializeSmsConfig(@NonNull String... smsSenderNumbers) {
        this.smsSenderNumbers = smsSenderNumbers;
    }

    @Nullable
    public String[] getSmsSenderNumbers() {
        return smsSenderNumbers;
    }
}
