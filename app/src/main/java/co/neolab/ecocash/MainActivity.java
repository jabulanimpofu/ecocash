package co.neolab.ecocash;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView statusText;
    private EditText editText;
    private Button button;
    private String amount;

    private static final String ECOCASH_PHONE_NUMBER = "+263164";
    private static final String USSD_ROOT = "*151*2*2";
    private static final String USSD_MERCHANT_CODE = "77319";

    @NonNull
    private LocalBroadcastManager localBroadcastManager;

    // A local broadcast receiver to receive payment confirmations from the SmsReceiver
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SmsReceiver.INTENT_ACTION_SMS)) {
                String receivedSender = intent.getStringExtra(SmsReceiver.KEY_SMS_SENDER);
                String receivedMessage = intent.getStringExtra(SmsReceiver.KEY_SMS_MESSAGE);
                String receivedAmountPaid = intent.getStringExtra(SmsReceiver.KEY_SMS_AMOUNT_PAID);
                String receivedMerchantCode = intent.getStringExtra(SmsReceiver.KEY_SMS_MERCHANT_CODE);
                String receivedTxnId = intent.getStringExtra(SmsReceiver.KEY_SMS_TXN_ID);

                //1. optimistically update user UI
                statusText.setText(getString(R.string.status_complete, receivedAmountPaid, receivedMerchantCode));

                //2. perform API call to submit the transaction data to the server
                //...
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialiseViews();

        //request permission to listen in on payment confirmation SMS messages
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionsTool.requestSMSPermission(MainActivity.this);
        }
    }

    private void initialiseViews() {
        statusText = (TextView) findViewById(R.id.payment_status);
        editText = (EditText) findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMakePayment();
            }
        });
        Config.INSTANCE.initializeSmsConfig(ECOCASH_PHONE_NUMBER);

        statusText.setText(getString(R.string.status_inactive));
    }

    @Override
    protected void onResume() {
        registerReceiver();
        super.onResume();
    }

    @Override
    protected void onPause() {
        unRegisterReceiver();
        super.onPause();
    }

    private void registerReceiver() {
        localBroadcastManager = LocalBroadcastManager.getInstance(MainActivity.this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsReceiver.INTENT_ACTION_SMS);
        localBroadcastManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    private void unRegisterReceiver() {
        localBroadcastManager.unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermissionsTool.REQUEST_CODE_ASK_PERMISSIONS_RECEIVE_SMS:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, getString(R.string.warning_permission_receive_sms_not_granted),
                            Toast.LENGTH_LONG).show();
                    startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.parse("package:" + getApplicationContext().getPackageName())));
                }
                return;
            case PermissionsTool.REQUEST_CODE_ASK_PERMISSIONS_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, getString(R.string.warning_permission_call_phone_not_granted),
                            Toast.LENGTH_LONG).show();
                    startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.parse("package:" + getApplicationContext().getPackageName())));
                } else {
                    //place call
                    placeCall();
                }
                return;
            }
        }
    }

    private void onMakePayment() {
        Log.d("ECOCASH:", "make payment");
        //get the value
        try {
            amount = "" + Integer.parseInt(editText.getText().toString());
        } catch (Exception e) {
            Log.d("ECOCASH", "Likely an invalid or non integral number has been given");
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkSelfPermission(Manifest.permission.CALL_PHONE);
            int permissionCheck = checkSelfPermission(Manifest.permission.CALL_PHONE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                PermissionsTool.requestCallPhonePermission(MainActivity.this);
            } else {
                placeCall();
            }
        } else {
            placeCall();
        }
    }

    private void placeCall() {
        //construct the USSD code
        final String ussd_code = String.format("%s*%s*%s%s", USSD_ROOT, USSD_MERCHANT_CODE, amount, Uri.encode("#"));
        Log.d("ECOCASH:", ussd_code);
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(String.format("tel:%s", ussd_code)));
        try {
            startActivity(intent);
            editText.setText(getString(R.string.status_in_progress, amount, USSD_MERCHANT_CODE));
        } catch (Exception e) {
            Log.d("ECOCASH:", "an error occurred while placing call");
            statusText.setText(getString(R.string.status_inactive));
        }
    }
}
